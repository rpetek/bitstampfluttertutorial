import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';

import '../models/currency.dart';

class RestApi {
    Client client = Client();

    static final String currencies = "currencies/";
    static final String baseUrl = "https://www.bitstamp.net/";
    static final String mobileApi = "api/mobile/v1/";
    final String currenciesUrl = "$baseUrl$mobileApi$currencies";

    bool isSuccessful(Response response) {
        return response.statusCode >= 200 || response.statusCode < 400;
    }

    Future<List<Currency>> getCurrencies() async {
        final response = await client.get(currenciesUrl);
        print(response.body.toString());
        if (isSuccessful(response)) {
            // If the call to the server was successful, parse the JSON
            CurrencyWrapper data = CurrencyWrapper.fromJson(
                json.decode(response.body));
            return data.currencies;
        } else {
            // If that call was not successful, throw an error.
            throw Exception('Failed to load currencies');
        }
    }
}
