import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'currency.dart';
import 'item_details.dart';

Widget buildItemRowCurrency(BuildContext context, Currency currency) {
    return Material(
        color: Colors.white,
        child: InkWell(
            onTap: () {
                Navigator.push(context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        duration: Duration(milliseconds: 250),
                        child: ItemDetailPage(currency: currency)
                    )
                );
            },
            child: Container(
                padding: const EdgeInsets.all(18),
                child: Row(
                    children: [
                        Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: Container(
                                height: 36,
                                child: Image.network(currency.logo)
                            )
                        ),
                        Expanded(
                            /*1*/
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    /*2*/
                                    Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 8),
                                        child: Text(
                                            currency.name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                            ),
                                        ),
                                    ),
                                    Text(
                                        currency.code,
                                        style: TextStyle(
                                            color: Colors.grey[500],
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ],
                ),
            ),
        ),
    );
}