import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'item_details.dart';

Widget buildItemRowInteractive(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
            onTap: () {
                Navigator.push(context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        duration: Duration(milliseconds: 250),
                        child: ItemDetailPage(title: "Bitcoin")
                    )
                );
            },
            child: Container(
                padding: const EdgeInsets.all(18),
                child: Row(
                    children: [
                        Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: Container(
                                height: 36,
                                child: Image.network(
                                    "https://www.bitstamp.net/s/apps/mobile/images/currency-logos/BTC@3x.png"),
                            )
                        ),
                        Expanded(
                            /*1*/
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    /*2*/
                                    Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 8),
                                        child: Text(
                                            'Bitcoin',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                            ),
                                        ),
                                    ),
                                    Text(
                                        '\$ 12235.99',
                                        style: TextStyle(
                                            color: Colors.grey[500],
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ],
                ),
            ),
        ),
    );
}