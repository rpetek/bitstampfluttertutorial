import 'package:flutter/material.dart';

import '../colors.dart' as Color;

class ItemDetailPage extends StatefulWidget {
    ItemDetailPage({Key key, this.title}) : super(key: key);

    final String title;

    @override
    ItemDetailPageState createState() => ItemDetailPageState();
}

class ItemDetailPageState extends State<ItemDetailPage> {

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                // Here we take the value from the MyHomePage object that was created by
                // the App.build method, and use it to set our appbar title.
                title: Text(widget.title),
                backgroundColor: Color.accent,
            ),
            body: SafeArea(
                child: ListView(
                    children: <Widget>[
                        const SizedBox(height: 50.0),
                        Container(
                            padding: const EdgeInsets.only(bottom: 26),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment
                                    .center,
                                children: [
                                    Container(
                                        height: 36,
                                        child: Image.network(
                                            "https://www.bitstamp.net/s/apps/mobile/images/currency-logos/BTC@3x.png"),
                                    ),
                                    const SizedBox(height: 16.0),
                                    Column(
                                        children: [
                                            /*2*/
                                            Container(
                                                padding: const EdgeInsets
                                                    .only(
                                                    bottom: 8),
                                                child: Text(
                                                    'Bitcoin',
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold,
                                                    ),
                                                ),
                                            ),
                                            Text(
                                                '\$ 12235.99',
                                                style: TextStyle(
                                                    color: Colors.grey[500],
                                                ),
                                            ),
                                        ],
                                    ),
                                ],
                            ),
                        ),
                        const Divider(height: 1),
                        Padding(
                            padding: EdgeInsets.all(18),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                    /*2*/
                                    Container(
                                        child: Text(
                                            'High',
                                            style: TextStyle(
                                                color: Colors.grey[500],
                                            ),
                                        ),
                                    ),
                                    Text(
                                        '\$ 12235.99',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                        Padding(
                            padding: EdgeInsets.all(18),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                    /*2*/
                                    Container(
                                        child: Text(
                                            'Low',
                                            style: TextStyle(
                                                color: Colors.grey[500],
                                            ),
                                        ),
                                    ),
                                    Text(
                                        '\$ 11335.62',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ],
                ),
            ),
        );
    }
}
