import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'colors.dart' as Color;
import 'part1/item_row.dart';
import 'part2/item_row.dart';
import 'part2/rest_api.dart';
import 'part3/currency.dart';
import 'part3/item_row.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        // material theme
        return MaterialApp(
            title: 'Bitstamp Futter',
            theme: ThemeData(
                // This is the theme of your application.
                primarySwatch: Colors.blue,
            ),
            home: MyHomePage(title: 'Bitstamp'),
        );
    }
}

class MyHomePage extends StatefulWidget {
    MyHomePage({Key key, this.title}) : super(key: key);

    // This widget is the home page of your application. It is stateful, meaning
    // that it has a State object (defined below) that contains fields that affect
    // how it looks.

    // Fields in a Widget subclass are always marked "final".

    // part 3 - Network requests
    final RestApi _restApi = RestApi();

    final String title;

    @override
    _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

    @override
    void initState() {
        super.initState();
    }

    // part 3
    List<Currency> _currencies = List();
    void _renderCurrencies(List<Currency> currencies) {
        setState(() {
            // This call to setState tells the Flutter framework that something has
            // changed in this State, which causes it to rerun the build method below
            // so that the display can reflect the updated values.
            _currencies = currencies;
        });
    }

    // part 2
    void _showToast() {
        Fluttertoast.showToast(
            msg: "Hello Flutter",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            fontSize: 16.0
        );
    }

    @override
    Widget build(BuildContext context) {
        // This method is rerun every time setState is called.
        // build methods are fast, so that you can just rebuild anything
        // that needs updating
        return Scaffold(
            backgroundColor: Colors.grey[100],
            appBar: AppBar(
                // Here we take the value from the MyHomePage
                title: Text(widget.title),
                backgroundColor: Color.accent,
            ),
            body: Center(
                // Center is a layout widget. It takes a single child and positions it
                // in the middle of the parent.
                child: Column(
                    // Column is also layout widget and it lists children
                    // vertically. It expands to be tall as its parent.
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        // part1 - Layout
                        //buildItemRow(),
                        // part2 - Interactivity
                        // buildItemRowInteractive(context)
                    ],
                ),
                /*child:
                // part 3 - Network requests
                ListView.builder(
                    itemCount: _currencies.length,
                    itemBuilder: (BuildContext context, int index) {
                        return buildItemRowCurrency(
                            context, _currencies[index]);
                    }),*/
            ),
            floatingActionButton: FloatingActionButton(
                backgroundColor: Color.accent,
                onPressed: () {
                    // part 2
                    _showToast();
                    // part 3
                    /*widget._restApi.getCurrencies().then((currencies) =>
                    {
                        _renderCurrencies(currencies)
                    });*/
                },
                tooltip: 'Toast',
                child: Icon(Icons.message),
            ),
        );
    }
}
