import 'package:flutter/material.dart';

Widget buildItemRow() {
    return Container(
        color: Colors.white,
        padding: const EdgeInsets.all(18),
        child: Row(
            children: [
                Padding(
                    padding: EdgeInsets.only(right: 18),
                    child: Container(
                        height: 36,
                        child: Image.network(
                            "https://www.bitstamp.net/s/apps/mobile/images/currency-logos/BTC@3x.png"),
                    )
                ),
                Expanded(
                    /*1*/
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            /*2*/
                            Container(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: Text(
                                    'Bitcoin',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                    ),
                                ),
                            ),
                            Text(
                                '\$ 12235.99',
                                style: TextStyle(
                                    color: Colors.grey[500],
                                ),
                            ),
                        ],
                    ),
                ),
            ],
        ),
    );
}